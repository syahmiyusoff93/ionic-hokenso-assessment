import Vue from "vue";
import App from "./App.vue";

import Ionic from "@ionic/vue";
import "@ionic/core/css/core.css";
import "@ionic/core/css/ionic.bundle.css";
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false;

import { IonicVueRouter } from "@ionic/vue";
import vuetify from './plugins/vuetify';
Vue.use(IonicVueRouter);
Vue.use(Ionic);
Vue.use(vuetify);

const router = new IonicVueRouter({
  mode: "history",
  base: "/",
  routes: [
    {
      path: "/tabs",
      component: () =>
        import(/* webpackChunkName: "home" */ "@/components/Home.vue"),
      children: [
        {
          path: "tab1",
          name: "tab1",
          components: {
            tab1: () =>
              import(/* webpackChunkName: "tab1" */ "@/components/Tab1.vue")
          }
        },
        {
          path: "tab1/details",
          name: "tab1-details",
          components: {
            tab1: () =>
              import(
                /* webpackChunkName: "tab1" */ "@/components/Tab1Details.vue"
              )
          }
        },
        {
          path: "tab2",
          name: "tab2",
          components: {
            tab2: () =>
              import(/* webpackChunkName: "tab2" */ "@/components/Tab2.vue")
          }
        },
        {
          path: "tab3",
          name: "tab3",
          components: {
            tab3: () =>
              import(/* webpackChunkName: "tab3" */ "@/components/Tab3.vue")
          }
        },
        {
          path: "tab4",
          name: "tab4",
          components: {
            tab4: () =>
              import(/* webpackChunkName: "tab4" */ "@/components/Tab4.vue")
          }
        }
      ]
    },
    { path: "/", redirect: "tabs/tab1" }
  ]
});
new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount("#app");
