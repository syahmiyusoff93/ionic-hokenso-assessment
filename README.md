# IONIC + VUEJS for Hokenso Assessment

## Sample Overview
![](appsview.gif)

## Project setup
1. Download all files from this repo 
2. `cd hokenso` 
3. open your terminal, (please make sure nodejs has been installed in your device).
4. `npm install` to install all of the dependancies for this project
5. `vue ui` to make you easier to execute this project and manage all vue plugins/projects
6. Go to `Task->serve` menu, and click `Run task` button and wait for a few seconds for it to build
7. App running at:
  - Local:   http://localhost:8080/ 
  - Network: http://<yourip>:8080/
